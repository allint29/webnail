﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebNail.Core;
using WebNail.Core.Domain.Store;
using WebNail.Core.Interfaces.Common.IHasProperties;
using WebNail.Core.Interfaces.Common.IRepositories;
using WebNail.DataAccess.EntityFramework;

namespace WebNail.DataAccess.Repositories
{
    public class EfRepository<T> :
        IRepository<T>
        where T : BaseEntity, IHasId
    {

        private readonly DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<ICollection<T>> GetAllAsync()
        {
            switch (typeof(T).Name)
            {
                case "ItemWork":
                    var itemWorks = await _dataContext.Set<ItemWork>().Include(e => e.ItemTags).ToListAsync();
                    if (itemWorks == null)
                        return null;

                    itemWorks.ForEach(iW =>
                    {
                        iW.ItemTagIds = new List<Guid>();
                        iW.ItemTags?.ToList().ForEach(r =>
                        {
                            //r.ItemWorks = null;
                            iW.ItemTagIds.Add(r.Id);
                        });
                    });

                    return itemWorks as ICollection<T>;

                case "ItemTag":
                    var itemTags = await _dataContext.Set<ItemTag>().Include(e => e.ItemWorks).ToListAsync();
                    if (itemTags == null)
                        return null;

                    itemTags.ForEach(iT =>
                    {
                        iT.ItemWorkIds = new List<Guid>();
                        iT.ItemWorks?.ToList().ForEach(r =>
                        {
                            //iT.ItemWorks = null
                            iT.ItemWorkIds.Add(r.Id);
                        });
                    });

                    return itemTags as ICollection<T>;
                default:
                    var defaultResult = await _dataContext.Set<T>().ToListAsync();

                    return defaultResult;
            }
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            switch (typeof(T).Name)
            {
                case "ItemWork":
                    var itemWork = await _dataContext.Set<ItemWork>().Include(e => e.ItemTags).FirstOrDefaultAsync(e => e.Id == id);
                    if (itemWork == null)
                        return null;

                    itemWork.ItemTagIds = new List<Guid>();
                    itemWork.ItemTags?.ToList().ForEach(r => itemWork.ItemTagIds.Add(r.Id));
                    return itemWork as T;

            
                case "ItemTag":
                    var itemTag = await _dataContext.Set<ItemTag>().Include(e => e.ItemWorks).FirstOrDefaultAsync(e => e.Id == id);
                    if (itemTag == null)
                        return null;

                    itemTag.ItemWorkIds = new List<Guid>();
                    itemTag.ItemWorks?.ToList().ForEach(r => itemTag.ItemWorkIds.Add(r.Id));
                    return itemTag as T;
                default:
                    var defaultResult = await _dataContext.Set<T>().FirstOrDefaultAsync(e => e.Id == id);

                    return defaultResult;
            }
        }

        public async Task<T> AddAsync(T entity)
        {
            if (entity == null)
                return null;

            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();

            return entity;
            // switch (typeof(T).Name)
            // {
            //     case "ItemWork":
            //         if (!(entity is ItemWork itemWork))
            //             return null;
            // 
            //         return itemWork as T;
            // 
            //     case "Role":
            //         if (!(entity is ItemTag itemTag))
            //             return null;
            // 
            //         return itemTag as T;
            //     default:
            //         if (!(entity is T tType))
            //             return null;
            // 
            //         return tType as T;
            // }
        }

        public async Task<T> UpdateAsync(T entity)
        {
            _dataContext.Update(entity);
            await _dataContext.SaveChangesAsync();

            return entity;
        }

        public async Task<T> DeleteAsync(Guid id)
        {
            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(e => e.Id == id);

            if (entity == null)
                return null;

            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();
            return entity;
        }

        public async Task<ICollection<T>> GetRangeByIdsAsync(ICollection<Guid> ids)
        {
            return await _dataContext.Set<T>().Where(d => ids.Any(id => id == d.Id)).ToListAsync();
        }

        public async Task AddRangeAsync(ICollection<T> entities)
        {
            await _dataContext.Set<T>().AddRangeAsync(entities);
            await _dataContext.SaveChangesAsync();
        }
    }
}
