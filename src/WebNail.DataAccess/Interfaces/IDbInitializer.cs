﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebNail.DataAccess.Interfaces
{
    public interface IDbInitializer
    {
        public void InitializeDb();

    }
}
