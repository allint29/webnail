﻿using System;
using WebNail.DataAccess.Interfaces;
using WebNail.DataAccess.EntityFramework;

namespace WebNail.DataAccess.Data
{
    public class EfDbInitializer: IDbInitializer
    {
        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        private DataContext _dataContext { get; }

        public void InitializeDb()
        {   
            // Убраля для миграции
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            var itemTags = FakeDataFactory.GetItemTags();
            var itemWorks = FakeDataFactory.GetItemWorks();

            _dataContext.AddRange(itemTags);
            _dataContext.AddRange(itemWorks);

            _dataContext.SaveChanges();

        }
    }
}
