﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualBasic;
using WebNail.Core.Domain.Store;

namespace WebNail.DataAccess.Data
{
    public static class FakeDataFactory
    {
        private static readonly List<string> Tags = new List<string>{"red", "green", "blue", "dark", "flowers", "love", "stickers", "trend"};


        private static readonly List<Guid> ItemTagIds = new List<Guid>();
        private static readonly List<Guid> ItemWorkIds = new List<Guid>();

        private static readonly List<int> YearList = new List<int>();
        private static readonly List<int> MonthList = new List<int>();
        private static readonly List<int> DayList = new List<int>();

        private static readonly List<DateTime> DateList = new List<DateTime>();

        private static List<ItemTag> ItemTagsList = new List<ItemTag>();
        private static List<ItemWork> ItemWorksList = new List<ItemWork>();

        static FakeDataFactory()
        {
            Start();
        }

        private static void Start()
        {
            for (int i = 0; i < 50; i++)
            {
                ItemTagIds.Add(Guid.NewGuid());
                ItemWorkIds.Add(Guid.NewGuid());

            }


            for (int i = 1953; i < 2013; i++)
            {
                YearList.Add(i);
            }

            for (int i = 1; i < 13; i++)
            {
                MonthList.Add(i);
            }

            for (int i = 1; i < 28; i++)
            {
                DayList.Add(i);
            }

            var rand = new Random();

            for (int i = 0; i < 50; i++)
            {

                DateTime date = new DateTime(YearList[rand.Next(0, 59)], MonthList[rand.Next(0, 11)], DayList[rand.Next(0, 27)]);

                DateList.Add(date);
            }

            var count = 0;

            foreach (var t in Tags)
            {
                ItemTagsList.Add(new ItemTag { Id = ItemTagIds[count], Name = t });
                count++;
            }

            count = 0;
            var countTags = Tags.Count - 1;

            ItemWorkIds.ForEach(id =>
            {
                var item = new ItemWork
                {
                    Id = id,
                    Comment = $"comment {id}",
                    Date = DateList[count],
                    Description = $"Description {count}",
                    PictureAddress = $"picture{count}.jpg",
                    ItemTags = new List<ItemTag>(),
                    ItemWorkItemTags = new List<ItemWorkItemTag>(),
                    ItemTagIds = new List<Guid>()
                };

                item.ItemTagIds.Add(ItemTagsList[countTags].Id);
                item.ItemTags.Add(ItemTagsList[countTags]);

                countTags--;

                if (countTags < 0)
                    countTags = Tags.Count - 1;

                ItemWorksList.Add(item);
                count++;
            });
        }

        public static List<ItemTag> GetItemTags()
        {
            if (!ItemWorksList.Any())
                Start();

            return ItemTagsList;
        }

        public static List<ItemWork> GetItemWorks()
        {
            if (!ItemWorksList.Any())
                Start();

            return ItemWorksList;
        }
    }
}
