﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebNail.Core.Domain.Store;

namespace WebNail.DataAccess.EntityFramework
{
    public class DataContext : DbContext
    {
        public DbSet<ItemWork> ItemWorks { get; set; }
        public DbSet<ItemTag> ItemTags { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ItemTagConfiguration());
            modelBuilder.ApplyConfiguration(new ItemWorkItemTagConfiguration());

            modelBuilder
                .Entity<ItemWork>()
                .HasMany(itemWork => itemWork.ItemTags)
                .WithMany(itemTag => itemTag.ItemWorks)
                .UsingEntity<ItemWorkItemTag>
                (
                    entityTypeBuilder => entityTypeBuilder
                        .HasOne(itemWorkItemTag => itemWorkItemTag.ItemTag)
                        .WithMany(itemTag => itemTag.ItemWorkItemTags)
                        .HasForeignKey(itemWorkItemTag => itemWorkItemTag.ItemTagId),
                    entityTypeBuilder => entityTypeBuilder
                        .HasOne(itemWorkItemTag => itemWorkItemTag.ItemWork)
                        .WithMany(itemWork => itemWork.ItemWorkItemTags)
                        .HasForeignKey(itemWorkItemTag => itemWorkItemTag.ItemWorkId),
                    entityTypeBuilder =>
                    {
                        //j.Property(cp => cp.TimeStamp).HasDefaultValueSql("CURRENT_TIMESTAMP");
                        //j.Property(pt => pt.Mark).HasDefaultValue(3);
                        entityTypeBuilder.HasKey(itemWorkItemTag => new { itemWorkItemTag.ItemWorkId, itemWorkItemTag.ItemTagId });
                        entityTypeBuilder.ToTable("ItemWorkItemTag");
                    }
                );
        }

        public class ItemTagConfiguration : IEntityTypeConfiguration<ItemTag>
        {
            public void Configure(EntityTypeBuilder<ItemTag> builder)
            {
                builder.HasKey(p => p.Id);
                builder.HasIndex(i => new { i.Name });
            }
        }

        public class ItemWorkItemTagConfiguration : IEntityTypeConfiguration<ItemWorkItemTag>
        {
            public void Configure(EntityTypeBuilder<ItemWorkItemTag> builder)
            {
                builder.HasAlternateKey(p => new { p.ItemTagId, p.ItemWorkId }).HasName("AlternativeKeyItemWorkItemTag");
                builder.HasIndex(i => new { i.ItemTagId, i.ItemWorkId });

            }
        }

    }
}
