﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebNail.Core.Application.Common.Exceptions;
using WebNail.Core.Application.Store.DtoModels;
using WebNail.Core.Interfaces.Common.IServices;

namespace WebNail.WebHost.Controllers
{
    /// <summary>
    /// Работы мастера
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ItemWorkController : Controller
    {

        private readonly IItemWorkService _itemWorkService;

        public ItemWorkController(IItemWorkService service)
        {
            _itemWorkService = service;
        }

        /// <summary>
        /// Получить данные всех работ мастера
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ICollection<ItemWorkResponse>> GetItemWorksAsync()
        {
            var items = await _itemWorkService.GetListAsync();
            return items;
        }


        /// <summary>
        /// Получить данные работы мастера по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<ItemWorkResponse>> GetItemWorkByIdAsync(Guid id)
        {
            ItemWorkResponse item = null;
            try
            {
                item = await _itemWorkService.GetByIdAsync(id);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }

            return item;
        }

        /// <summary>
        /// Создание записи о работе мастера
        /// </summary>
        /// <param name="request">Класс запроса для создания записи о работе мастера или их изменения</param>
        /// <returns>Данные о работе мастера</returns>
        [HttpPost]
        public async Task<ActionResult<ItemWorkResponse>> CreateItemWorkAsync(ItemWorkCreateOrEdit request)
        {
            var item = await _itemWorkService.UpdateAsync(request);

            return CreatedAtAction(nameof(CreateItemWorkAsync), new { id = item.Id }, item.Id);
        }

        /// <summary>
        /// Изменение данных работы мастера
        /// </summary>
        /// <param name="id">Уникальный идентификатор работы</param>
        /// <param name="request">Класс запроса для создания записи о работе мастера или их изменения</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<ItemWorkResponse>> EditItemWorkAsync(Guid id, ItemWorkCreateOrEdit request)
        {
            if (string.IsNullOrEmpty(request.Id) || request.Id == null)
                request.Id = id.ToString();

            var result = await _itemWorkService.UpdateAsync(request);
            return result;
        }

        /// <summary>
        /// Удалить данные о работе мастера
        /// </summary>
        /// <param name="id">Уникальный идентификатор работы</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<ItemWorkResponse>> DeleteItemWorkAsync(Guid id)
        {
            var result =  await _itemWorkService.DeleteAsync(id);
            return result;
        }
    }
}
