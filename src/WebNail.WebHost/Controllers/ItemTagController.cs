﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebNail.Core.Application.Common.Exceptions;
using WebNail.Core.Application.Store.DtoModels;
using WebNail.Core.Interfaces.Common.IServices;

namespace WebNail.WebHost.Controllers
{
    /// <summary>
    /// Тэги для работ мастера
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ItemTagController : Controller
    {

        private readonly IItemTagService _itemTagService;

        public ItemTagController(IItemTagService service)
        {
            _itemTagService = service;
        }

        /// <summary>
        /// Получить данные всех тегов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ICollection<ItemTagResponse>> GetItemTagsAsync()
        {
            var items = await _itemTagService.GetListAsync();
            return items;
        }

        /// <summary>
        /// Получить данные тега по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<ItemTagResponse>> GetItemTagByIdAsync(Guid id)
        {
            ItemTagResponse item = null;
            try
            {
                item = await _itemTagService.GetByIdAsync(id);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }
            return item;
        }


        /// <summary>
        /// Создание записи о теге
        /// </summary>
        /// <param name="request">Класс запроса для создания записи о теге или их изменения</param>
        /// <returns>Данные о теге</returns>
        [HttpPost]
        public async Task<ActionResult<ItemTagResponse>> CreateItemTagAsync(ItemTagCreateOrEdit request)
        {
            var item = await _itemTagService.UpdateAsync(request);

            return CreatedAtAction(nameof(CreateItemTagAsync), new { id = item.Id }, item.Id);
        }
        
        /// <summary>
        /// Изменение данных тега
        /// </summary>
        /// <param name="id">Уникальный идентификатор тега</param>
        /// <param name="request">Класс запроса для создания записи о теге или их изменения</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<ItemTagResponse>> EditItemTagAsync(Guid id, ItemTagCreateOrEdit request)
        {
            if (string.IsNullOrEmpty(request.Id) || request.Id == null)
                request.Id = id.ToString();

            var result = await _itemTagService.UpdateAsync(request);

            return result;
            //return Ok();
        }


        /// <summary>
        /// Удалить данные о теге
        /// </summary>
        /// <param name="id">Уникальный идентификатор тега</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<ItemTagResponse>> DeleteItemTagAsync(Guid id)
        {
            var result = await _itemTagService.DeleteAsync(id);
            return result;
        }
    }
}
