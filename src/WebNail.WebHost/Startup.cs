using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebNail.Core.Application.Store.Services;
using WebNail.Core.Domain.Common;
using WebNail.Core.Interfaces.Common.IProviders;
using WebNail.Core.Interfaces.Common.IRepositories;
using WebNail.Core.Interfaces.Common.IServices;
using WebNail.DataAccess.Data;
using WebNail.DataAccess.EntityFramework;
using WebNail.DataAccess.Interfaces;
using WebNail.DataAccess.Repositories;

namespace WebNail.WebHost
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public IConfiguration Configuration { get; }


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Cross domain specification
            services.AddCors(opt =>
            {
                opt.AddPolicy(name: MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder
                            //.WithOrigins("http://localhost:4200",
                            //"http://localhost:4200/employees")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowAnyOrigin()
                            //.AllowCredentials()
                            ;
                    });
            });


            services.AddMvcCore();

            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve);
            //services.AddControllers().AddNewtonsoftJson(x =>
            //    x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IItemWorkService, ItemWorkService>();
            services.AddScoped<IItemTagService, ItemTagService>();
            

            services.AddTransient<ICurrentDateTimeProvider, CurrentDateTimeProvider>();

            services.AddDbContext<DataContext>(
                x =>
                {
                    //x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
                    //x.UseLazyLoadingProxies();
                    //x.UseNpgsql(builder.ToString());
                    //x.UseInMemoryDatabase()
                    x.UseNpgsql(Configuration.GetConnectionString("NpgDb"));
                    x.EnableSensitiveDataLogging();
                }
                ,
                ServiceLifetime.Singleton
            );

            services.AddOpenApiDocument(options =>
            {
                options.Title = "WebNail API Documentation";
                options.Version = "1.0";
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer initializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(MyAllowSpecificOrigins);
            app.UseOpenApi();

            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            initializer.InitializeDb();
        }
    }
}
