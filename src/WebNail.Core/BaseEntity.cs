﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebNail.Core
{
    /// <summary>
    /// Абстрактный класс для работы с базой данных
    /// </summary>
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }

    }
}
