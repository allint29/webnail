﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WebNail.Core.Interfaces.Common.IHasProperties;

namespace WebNail.Core.Domain.Store
{
    /// <summary>
    /// Класс сортировки работ по тегам
    /// </summary>
    public class ItemTag: BaseEntity, IHasId
    {
        public string Name { get; set; }

        [NotMapped]
        public ICollection<Guid> ItemWorkIds { get; set; }

        public ICollection<ItemWork> ItemWorks { get; set; }

        [NotMapped]
        public ICollection<ItemWorkItemTag> ItemWorkItemTags { get; set; }
    }
}
