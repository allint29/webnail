﻿using System;
using System.Collections.Generic;
using System.Text;
using WebNail.Core.Interfaces.Common.IHasProperties;

namespace WebNail.Core.Domain.Store
{
    public class ItemWorkItemTag: BaseEntity
    {
        public Guid ItemWorkId { get; set; }
        public ItemWork ItemWork { get; set; }
        public Guid ItemTagId { get; set; }
        public ItemTag ItemTag { get; set; }
    }
}
