﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WebNail.Core.Interfaces.Common.IHasProperties;

namespace WebNail.Core.Domain.Store
{
    /// <summary>
    /// Работа мастера. Содержит: дату, описание, теги, комментарии 
    /// </summary>
    public class ItemWork : BaseEntity, IHasId, IHasPictureAddress
    {
        /// <summary>
        /// Ссылка на картинку работы
        /// </summary>
        public string PictureAddress { get; set; }

        /// <summary>
        /// Дата работы
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Описание выставленной работы мастером
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Комментарий, который может оставлять пользователь
        /// </summary>
        public string Comment { get; set; }

        [NotMapped]
        public ICollection<Guid> ItemTagIds { get; set; }
        /// <summary>
        /// Тег для поиска по тегам
        /// </summary>
        public ICollection<ItemTag> ItemTags { get; set; }

        [NotMapped]
        public ICollection<ItemWorkItemTag> ItemWorkItemTags { get; set; }
    }
}
