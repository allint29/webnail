﻿using System;
using System.Collections.Generic;
using System.Text;
using WebNail.Core.Interfaces.Common.IProviders;

namespace WebNail.Core.Domain.Common
{
    public class CurrentDateTimeProvider
        : ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}
