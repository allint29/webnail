﻿using System;
using System.Collections.Generic;
using System.Text;
using WebNail.Core.Domain.Store;

namespace WebNail.Core.Application.Store.DtoModels
{
    public class ItemTagResponse: ItemTagShortResponse
    {
        public ICollection<ItemWorkShortResponse> ItemWorks { get; set; }
    }
}
