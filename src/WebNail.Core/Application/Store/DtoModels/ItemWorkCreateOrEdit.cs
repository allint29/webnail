﻿using System;
using System.Collections.Generic;
using System.Text;
using WebNail.Core.Domain.Store;

namespace WebNail.Core.Application.Store.DtoModels
{
    public class ItemWorkCreateOrEdit
    {
        public string Id { get; set; }
        /// <summary>
        /// Ссылка на картинку работы
        /// </summary>
        public string PictureAddress { get; set; }

        /// <summary>
        /// Дата работы
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Описание выставленной работы мастером
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Комментарий, который может оставлять пользователь
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Тег для поиска по тегам
        /// </summary>
        public ICollection<ItemTagCreateOrEdit> ItemTags { get; set; } = new List<ItemTagCreateOrEdit>();
    }
}
