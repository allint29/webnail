﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebNail.Core.Application.Store.DtoModels
{
    public class ItemWorkShortResponse
    {
        public string Id { get; set; }
        /// <summary>
        /// Ссылка на картинку работы
        /// </summary>
        public string PictureAddress { get; set; }

        /// <summary>
        /// Дата работы
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Описание выставленной работы мастером
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Комментарий, который может оставлять пользователь
        /// </summary>
        public string Comment { get; set; }
    }
}
