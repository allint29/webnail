﻿using System;
using System.Collections.Generic;
using System.Text;
using WebNail.Core.Domain.Store;

namespace WebNail.Core.Application.Store.DtoModels
{
    public class ItemWorkResponse: ItemWorkShortResponse
    {

        /// <summary>
        /// Тег для поиска по тегам
        /// </summary>
        public ICollection<ItemTagShortResponse> ItemTags { get; set; }
    }
}
