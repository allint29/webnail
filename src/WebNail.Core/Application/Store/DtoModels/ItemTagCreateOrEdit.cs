﻿using System;
using System.Collections.Generic;
using System.Text;
using WebNail.Core.Domain.Store;

namespace WebNail.Core.Application.Store.DtoModels
{
    public class ItemTagCreateOrEdit
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ICollection<ItemWorkCreateOrEdit> ItemWorks { get; set; }
    }
}
