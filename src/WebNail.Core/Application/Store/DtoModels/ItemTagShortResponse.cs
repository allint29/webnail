﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebNail.Core.Application.Store.DtoModels
{
    public class ItemTagShortResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
