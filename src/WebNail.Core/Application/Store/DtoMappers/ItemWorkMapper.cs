﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebNail.Core.Application.Store.DtoModels;
using WebNail.Core.Domain.Store;

namespace WebNail.Core.Application.Store.DtoMappers
{
    /// <summary>
    /// Класс сопоставления запроса/ответа с моделью работы мастера и наоборот
    /// </summary>
    public static class ItemWorkMapper
    {
        public static ItemWork MapToModel(ItemWorkCreateOrEdit requestItem, ItemWork result = null)
        {
            if (requestItem == null)
                return null;

            DateTime date = DateTime.Now;
            DateTime.TryParse(requestItem.Date, out date);


            if (result == null)
            {
                result = new ItemWork
                {
                    Comment = requestItem.Comment,
                    Description = requestItem.Description,
                    PictureAddress = requestItem.PictureAddress,
                    Date = date
                };
            }
            else
            {
                result.Comment = requestItem.Comment;
                result.Description = requestItem.Description;
                result.PictureAddress = requestItem.PictureAddress;
                result.Date = date;
            }

            if (!string.IsNullOrEmpty(requestItem.Id))
            {
                Guid id = Guid.Empty;
                if (Guid.TryParse(requestItem.Id, out id))
                    result.Id = id;
            }

            var tags = requestItem.ItemTags?.ToList();
            result.ItemTags = new List<ItemTag>();
            if (tags?.Count() > 0)
                tags.ForEach(i => result.ItemTags.Add(ItemTagMapper.MapToModel(i)));

            return result;
        }

        public static ItemWorkResponse MapFromModel(ItemWork model, bool withShortTags = false)
        {
            if (model == null)
                return null;

            var result = new ItemWorkResponse
            {
                Comment = model.Comment,
                Description = model.Description,
                PictureAddress = model.PictureAddress,
                Date = model.Date.ToString("yyyy-MM-dd"),
                Id = model.Id == Guid.Empty ? string.Empty : model.Id.ToString(),
                ItemTags = new List<ItemTagShortResponse>()
            };


            if (withShortTags) 
                return result;

            var tags = model.ItemTags?.ToList();
            if (tags?.Count() > 0)
            {
                tags.ForEach(t =>
                {
                    var tagResponse = ItemTagMapper.MapFromModel(t, true);
                    result.ItemTags.Add(tagResponse);
                });
            }

            return result;
        }
    }
}
