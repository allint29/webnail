﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebNail.Core.Application.Store.DtoModels;
using WebNail.Core.Domain.Store;

namespace WebNail.Core.Application.Store.DtoMappers
{
    public static class ItemTagMapper
    {
        public static ItemTag MapToModel(ItemTagCreateOrEdit requestItem, ItemTag result = null)
        {
            if (requestItem == null)
                return null;

            if (result == null)
            {
                result = new ItemTag
                {
                    Name = requestItem.Name,
                    ItemWorks = new List<ItemWork>()

                };
            }
            else
            {
                result.Name = requestItem.Name;
                result.ItemWorks = new List<ItemWork>();
            }


            if (!string.IsNullOrEmpty(requestItem.Id))
            {
                var id = Guid.Empty;
                if (Guid.TryParse(requestItem.Id, out id))
                    result.Id = id;
            }

            var works = requestItem.ItemWorks?.ToList();
            if (works?.Count() > 0)
            {
                works.ForEach(i => result.ItemWorks.Add(ItemWorkMapper.MapToModel(i)));
            }
               

            return result;
        }

        public static ItemTagResponse MapFromModel(ItemTag model, bool withShortWorks = false)
        {
            if (model == null)
                return null;

            var result = new ItemTagResponse
            {
                Name = model.Name,
                ItemWorks = new List<ItemWorkShortResponse>(),
                Id = model.Id == Guid.Empty ? string.Empty : model.Id.ToString()

            };

            if (withShortWorks)
                return result;

            var works = model.ItemWorks?.ToList();
            if (works?.Count() > 0)
            {
                works.ForEach(w =>
                {
                    var workResponse = ItemWorkMapper.MapFromModel(w, true);
                    result.ItemWorks.Add(workResponse);
                });
            }

            return result;
        }
    }
}
