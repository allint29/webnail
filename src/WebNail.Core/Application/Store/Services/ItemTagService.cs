﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebNail.Core.Application.Common.Exceptions;
using WebNail.Core.Application.Store.DtoMappers;
using WebNail.Core.Application.Store.DtoModels;
using WebNail.Core.Domain.Store;
using WebNail.Core.Interfaces.Common.IRepositories;
using WebNail.Core.Interfaces.Common.IServices;

namespace WebNail.Core.Application.Store.Services
{
    public class ItemTagService: IItemTagService
    {
        private readonly IRepository<ItemTag> _itemTagRepository;
        private readonly IRepository<ItemWork> _itemWorkRepository;

        public ItemTagService(
            IRepository<ItemTag> itemTagRepository,
            IRepository<ItemWork> itemWorkRepository
            /*INotificationGateway notification*/)
        {
            _itemTagRepository = itemTagRepository;
            _itemWorkRepository = itemWorkRepository;
            // _notification = notification;
        }
        public async Task<ItemTagResponse> GetByIdAsync(Guid id)
        {
            var itemResult = await _itemTagRepository.GetByIdAsync(id);

            if (itemResult == null)
                throw new EntityNotFoundException(id);

            var result = ItemTagMapper.MapFromModel(itemResult);

            return result;
        }

        public async Task<ICollection<ItemTagResponse>> GetListAsync()
        {
            var itemList = await _itemTagRepository.GetAllAsync();
            List<ItemTagResponse> itemsResponse = new List<ItemTagResponse>();

            itemList.ToList().ForEach(i => itemsResponse.Add(ItemTagMapper.MapFromModel(i)));
            return itemsResponse;
        }

        public async Task<ItemTagResponse> UpdateAsync(ItemTagCreateOrEdit request)
        {

            if (request == null)
                throw new NullRequestArgumentException();

            ItemTagResponse result = null;
            ItemTag addedItem = null;
            
            if (request.Id == string.Empty)
            {
                addedItem = ItemTagMapper.MapToModel(request);

                if (addedItem == null)
                    throw new NullRequestArgumentException();
                addedItem.ItemWorks = await GetRangeItemWorksByIdsAsync(addedItem.ItemWorks.ToList());
                await _itemTagRepository.AddAsync(addedItem);
            }
            else
            {
                Guid.TryParse(request.Id, out var id);

                addedItem = await _itemTagRepository.GetByIdAsync(id);

                if (addedItem == null)
                {
                    addedItem = ItemTagMapper.MapToModel(request);
                    addedItem.ItemWorks = await GetRangeItemWorksByIdsAsync(addedItem.ItemWorks.ToList());
                    await _itemTagRepository.AddAsync(addedItem);
                }
                else
                {
                    addedItem = ItemTagMapper.MapToModel(request, addedItem);
                    addedItem.ItemWorks = await GetRangeItemWorksByIdsAsync(addedItem.ItemWorks.ToList());
                    await _itemTagRepository.UpdateAsync(addedItem);
                }
            }

            result = ItemTagMapper.MapFromModel(addedItem);
            return result;
        }

        public async Task<ItemTagResponse> DeleteAsync(Guid id)
        {
            if (id == Guid.Empty)
                throw new NullRequestArgumentException();

            ItemTagResponse result = null;

            var existedItem = await _itemTagRepository.GetByIdAsync(id);

            if (existedItem != null)
            {
                var deleted = await _itemTagRepository.DeleteAsync(id);
                result = ItemTagMapper.MapFromModel(deleted);
            }

            return result;
        }

        public async Task<ICollection<ItemWork>> GetRangeItemWorksByIdsAsync(List<ItemWork> list)
        {
            var result = new List<ItemWork>();

            var listItemWorksHasId = list.Where(x => !x.Id.Equals(Guid.Empty)).ToList();
            var listIds = listItemWorksHasId.Select(x => x.Id).ToList();

            if (listIds?.Count() > 0)
                result.AddRange(await _itemWorkRepository.GetRangeByIdsAsync(listIds));

            var listToAdd = list.Where(x => result.Any(y => x.Id.Equals(Guid.Empty))).ToList();

            if (listToAdd?.Count() > 0)
            {
                //await _itemTagRepository.AddRangeAsync(listToAdd);
                result.AddRange(listToAdd);
            }

            return result;
        }
    }
}
