﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebNail.Core.Application.Common.Exceptions;
using WebNail.Core.Application.Store.DtoMappers;
using WebNail.Core.Application.Store.DtoModels;
using WebNail.Core.Domain.Store;
using WebNail.Core.Interfaces.Common.IRepositories;
using WebNail.Core.Interfaces.Common.IServices;

namespace WebNail.Core.Application.Store.Services
{
    /// <summary>
    /// Сервис для работы с хранилищем itemWork
    /// </summary>
    public class ItemWorkService: IItemWorkService
    {
        private readonly IRepository<ItemWork> _itemWorkRepository;
        private readonly IRepository<ItemTag> _itemTagRepository;

        public ItemWorkService(
            IRepository<ItemWork> itemWorkRepository,
            IRepository<ItemTag> itemTagRepository
            /*INotificationGateway notification*/)
        {
            _itemWorkRepository = itemWorkRepository;
            _itemTagRepository = itemTagRepository;
            // _notification = notification;
        }

        /// <summary>
        /// Получение из базы по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ItemWorkResponse> GetByIdAsync(Guid id)
        {
            var itemResult = await _itemWorkRepository.GetByIdAsync(id);

            if (itemResult == null)
                throw new EntityNotFoundException(id);

            var result = ItemWorkMapper.MapFromModel(itemResult);

            return result;
        }

        /// <summary>
        /// Получение из базы всех item
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<ItemWorkResponse>> GetListAsync()
        {
            var itemsList = await _itemWorkRepository.GetAllAsync();
            List<ItemWorkResponse> itemsResponse = new List<ItemWorkResponse>();

            itemsList.ToList().ForEach(i => itemsResponse.Add(ItemWorkMapper.MapFromModel(i)));
            return itemsResponse;
        }

        /// <summary>
        /// Измененние item в базе
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ItemWorkResponse> UpdateAsync(ItemWorkCreateOrEdit request)
        {
            if (request == null)
                throw new NullRequestArgumentException();

            ItemWorkResponse result = null;
            ItemWork addedItem = null;


            if (request.Id == string.Empty)
            {
                addedItem = ItemWorkMapper.MapToModel(request);

                if (addedItem == null)
                    throw new NullRequestArgumentException();
                addedItem.ItemTags = await GetRangeItemTagsByIdsAsync(addedItem.ItemTags.ToList());
                await _itemWorkRepository.AddAsync(addedItem);
            }
            else
            {
                Guid.TryParse(request.Id, out var id);

                addedItem = await _itemWorkRepository.GetByIdAsync(id);

                if (addedItem == null)
                {
                    addedItem = ItemWorkMapper.MapToModel(request);
                    addedItem.ItemTags = await GetRangeItemTagsByIdsAsync(addedItem.ItemTags.ToList());
                    await _itemWorkRepository.AddAsync(addedItem);
                }
                else
                {
                    addedItem = ItemWorkMapper.MapToModel(request, addedItem);
                    addedItem.ItemTags = await GetRangeItemTagsByIdsAsync(addedItem.ItemTags.ToList());
                    await _itemWorkRepository.UpdateAsync(addedItem);
                }
            }

            result = ItemWorkMapper.MapFromModel(addedItem);
            return result;

        }

        public async Task<ItemWorkResponse> DeleteAsync(Guid id)
        {
            if (id == Guid.Empty)
                throw new NullRequestArgumentException();

            ItemWorkResponse result = null;

            var existedItem = await _itemWorkRepository.GetByIdAsync(id);

            if (existedItem != null)
            {
                var deleted = await _itemWorkRepository.DeleteAsync(id);
                result = ItemWorkMapper.MapFromModel(deleted);
            }

            return result;
        }

        public async Task<ICollection<ItemTag>> GetRangeItemTagsByIdsAsync(List<ItemTag> list)
        {
            var result = new List<ItemTag>();
            
            var listItemWorksHasId = list.Where(x => !x.Id.Equals(Guid.Empty)).ToList();
            var listIds = listItemWorksHasId.Select(x => x.Id).ToList();

            if (listIds?.Count() > 0)
                result.AddRange(await _itemTagRepository.GetRangeByIdsAsync(listIds));


            var listToAdd = list.Where(x => result.Any(y => x.Id.Equals(Guid.Empty))).ToList();

            if (listToAdd?.Count() > 0)
            {
                //await _itemTagRepository.AddRangeAsync(listToAdd);
                result.AddRange(listToAdd);
            }

            return result;
        }

    }
}
