﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace WebNail.Core.Application.Common.Exceptions
{
    class NullRequestArgumentException : Exception
    {
        const string mess = "Ошибка. Запрос сохранения пустого объекта.";
        public NullRequestArgumentException() : base(mess)
        {

        }

    }
}
