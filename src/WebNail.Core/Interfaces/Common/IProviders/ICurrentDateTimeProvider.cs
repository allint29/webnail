﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebNail.Core.Interfaces.Common.IProviders
{
    public interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}
