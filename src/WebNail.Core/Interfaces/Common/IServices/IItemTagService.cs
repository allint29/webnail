﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebNail.Core.Application.Store.DtoModels;

namespace WebNail.Core.Interfaces.Common.IServices
{
    public interface IItemTagService: IRestService<ItemTagResponse, ItemTagCreateOrEdit>
    {
    }
}
