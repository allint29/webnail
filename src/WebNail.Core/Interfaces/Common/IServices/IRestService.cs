﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebNail.Core.Application.Store.DtoModels;

namespace WebNail.Core.Interfaces.Common.IServices
{
    public interface IRestService<T, in TV>
    {
        public Task<T> GetByIdAsync(Guid id);
        public Task<ICollection<T>> GetListAsync();
        public Task<T> UpdateAsync(TV request);
        public Task<T> DeleteAsync(Guid id); 
        // public Task<ICollection<T>> GetRangeByIdsAsync(ICollection<Guid> listIds);

    }
}
