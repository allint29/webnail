﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebNail.Core.Interfaces.Common.IHasProperties
{
    public interface IHasPictureAddress
    {
        public string PictureAddress { get; set; }
    }
}
