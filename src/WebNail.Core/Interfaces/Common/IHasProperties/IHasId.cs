﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebNail.Core.Interfaces.Common.IHasProperties
{
    /// <summary>
    /// Интерфейс для работы с базой данных
    /// </summary>
    public interface IHasId
    {
        public Guid Id { get; set; }
    }
}
