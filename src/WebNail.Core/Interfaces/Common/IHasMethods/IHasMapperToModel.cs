﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebNail.Core.Interfaces.Common.IHasMethods
{
    public interface IHasMapperToModel
    {
        public object MapToModel();
    }
}
