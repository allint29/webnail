﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebNail.Core.Interfaces.Common.IHasProperties;

namespace WebNail.Core.Interfaces.Common.IRepositories
{
    public interface IRepository<T> where T
        : BaseEntity, IHasId
    {
        public Task<ICollection<T>> GetAllAsync();
        public Task<T> GetByIdAsync(Guid id);
        public Task<T> AddAsync(T entity);
        public Task<T> UpdateAsync(T entity);
        public Task<T> DeleteAsync(Guid id);
        public Task<ICollection<T>> GetRangeByIdsAsync(ICollection<Guid> ids);
        public Task AddRangeAsync(ICollection<T> entities);
        /*
        
        
        public Task DeleteRangeAsync(IEnumerable<T> entities);
        
        */
    }
}
